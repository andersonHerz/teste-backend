package br.com.texoit.testebackend.model;

import java.util.List;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "producer")
public class Producer {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;

	@NotNull
	private String name;

	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "movie_producer", joinColumns = {
			@JoinColumn(referencedColumnName = "id", name = "producer_id") }, inverseJoinColumns = {
					@JoinColumn(referencedColumnName = "id", name = "movie_id") })
	private List<Movie> movies;

	public Producer() {
		super();
	}

	public Producer(@NotNull String name) {
		super();
		this.name = name;
	}

	public UUID getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Movie> getMovies() {
		return movies;
	}

	public void setMovies(List<Movie> movies) {
		this.movies = movies;
	}

}
