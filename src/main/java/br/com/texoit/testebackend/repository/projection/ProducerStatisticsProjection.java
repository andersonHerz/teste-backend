package br.com.texoit.testebackend.repository.projection;

import java.util.List;

public class ProducerStatisticsProjection {

	private List<ProducerStatistics> min;
	private List<ProducerStatistics> max;

	public ProducerStatisticsProjection(List<ProducerStatistics> min, List<ProducerStatistics> max) {
		super();
		this.min = min;
		this.max = max;
	}

	public List<ProducerStatistics>  getMin() {
		return min;
	}

	public void setMin(List<ProducerStatistics> min) {
		this.min = min;
	}

	public List<ProducerStatistics> getMax() {
		return max;
	}

	public void setMax(List<ProducerStatistics> max) {
		this.max = max;
	}

}
