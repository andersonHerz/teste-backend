package br.com.texoit.testebackend.configuration.property;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import br.com.texoit.testebackend.initialize.ApplicationInitializer;

@Configuration
@ConfigurationProperties("api")
public class ApplicationApiProperty {

	@Autowired
	private ApplicationInitializer appInitializer;

	private String locationFile = "classpath:movieslist/movielist.csv";

	public String getLocationFile() {
		return locationFile;
	}

	public void setLocationFile(String locationFile) {
		this.locationFile = locationFile;
	}

	@PostConstruct
	public void afterPropertiesSet() {
		appInitializer.initialize(this.getLocationFile());
	}

}
