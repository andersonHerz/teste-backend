package br.com.texoit.testebackend.model;

import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "movie")
public class Movie {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;

	@NotNull
	private Integer year;

	@NotNull
	private String title;

	@NotNull
	private String studios;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "movie_producer", joinColumns = {
			@JoinColumn(referencedColumnName = "id", name = "movie_id") }, inverseJoinColumns = {
					@JoinColumn(referencedColumnName = "id", name = "producer_id") })
	private List<Producer> producers;

	@NotNull
	private Boolean winner;

	public Movie() {
		super();
	}

	public Movie(@NotNull Integer year, @NotNull String title, @NotNull String studios,
			@NotNull List<Producer> producers, @NotNull Boolean winner) {
		super();
		this.year = year;
		this.title = title;
		this.studios = studios;
		this.producers = producers;
		this.winner = winner;
	}

	public UUID getId() {
		return id;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStudios() {
		return studios;
	}

	public void setStudios(String studios) {
		this.studios = studios;
	}

	public List<Producer> getProducers() {
		return producers;
	}

	public void setProducers(List<Producer> producers) {
		this.producers = producers;
	}

	public Boolean isWinner() {
		return winner;
	}

	public void setWinner(Boolean winner) {
		this.winner = winner;
	}

	@Override
	public String toString() {
		return "[Year= " + getYear() + //
				" , title= " + getTitle() + //
				" , studios= " + getStudios() + //
				" , winner= " + isWinner() +
				"]";
	}

}
