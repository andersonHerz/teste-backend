package br.com.texoit.testebackend.initialize;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.ResourceUtils;

import com.opencsv.exceptions.CsvException;

@SpringBootTest
public class ApplicationInitializerValidatorTest {

	private static final String ARQUIVO_OK = "classpath:arquivoOk.csv";
	private static final String ARQUIVO_ERRO_CABEÇALHO_QUANTIDADE = "classpath:erroCabecalhoQuantidade.csv";
	private static final String ARQUIVO_ERRO_CABEÇALHO_CAMPOS = "classpath:erroCabecalhoCampos.csv";
	private static final String ARQUIVO_ERRO_QUANTIDADE_CAMPOS = "classpath:erroQuantidadeCampos.csv";
	private static final String ARQUIVO_ERRO_TIPO_CAMPO_YEAR = "classpath:erroTipoCampoYear.csv";
	private static final String ARQUIVO_ERRO_TIPO_CAMPO_WINNER = "classpath:erroTipoCampoWinner.csv";

	@Test
	void testArquivoOk() throws IOException, CsvException {
		File file = ResourceUtils.getFile(ARQUIVO_OK);

		assertTrue(ApplicationInitializerValidator.validate(file), "Falha na validação, deve ser considerado valido.");
	}

	@Test
	void testeArquivoErroCabecalhoQuantidade() throws IOException, CsvException {
		File file = ResourceUtils.getFile(ARQUIVO_ERRO_CABEÇALHO_QUANTIDADE);

		assertFalse(ApplicationInitializerValidator.validate(file),
				"Falha na validação, deve ser considerado invalido.");
	}
	
	@Test
	void testeArquivoErroCabecalhoCampo() throws IOException, CsvException {
		File file = ResourceUtils.getFile(ARQUIVO_ERRO_CABEÇALHO_CAMPOS);

		assertFalse(ApplicationInitializerValidator.validate(file),
				"Falha na validação, deve ser considerado invalido.");
	}

	@Test
	void testeArquivoErroQuantidadeCampos() throws IOException, CsvException {
		File file = ResourceUtils.getFile(ARQUIVO_ERRO_QUANTIDADE_CAMPOS);

		assertFalse(ApplicationInitializerValidator.validate(file),
				"Falha na validação, deve ser considerado invalido.");
	}

	@Test
	void testeArquivoErroTipoCampoYear() throws IOException, CsvException {
		File file = ResourceUtils.getFile(ARQUIVO_ERRO_TIPO_CAMPO_YEAR);

		assertFalse(ApplicationInitializerValidator.validate(file),
				"Falha na validação, deve ser considerado invalido.");
	}

	@Test
	void testeArquivoErroTipoCampoWinner() throws IOException, CsvException {
		File file = ResourceUtils.getFile(ARQUIVO_ERRO_TIPO_CAMPO_WINNER);

		assertFalse(ApplicationInitializerValidator.validate(file),
				"Falha na validação, deve ser considerado invalido.");
	}

}
