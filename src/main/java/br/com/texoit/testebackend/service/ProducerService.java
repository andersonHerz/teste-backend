package br.com.texoit.testebackend.service;

import java.util.List;
import java.util.UUID;

import br.com.texoit.testebackend.model.Producer;
import br.com.texoit.testebackend.repository.projection.ProducerStatisticsProjection;

public interface ProducerService {

	public Producer findByNameOrElseCreate(String name);

	public List<Producer> findAllById(List<UUID> ids);

	public ProducerStatisticsProjection listStatisticsWinners();

}
