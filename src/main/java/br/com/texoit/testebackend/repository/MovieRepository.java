package br.com.texoit.testebackend.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.texoit.testebackend.model.Movie;

public interface MovieRepository extends JpaRepository<Movie, UUID> {

	public List<Movie> findByWinnerTrue();
	
}
