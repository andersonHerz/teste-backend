package br.com.texoit.testebackend.repository.projection;

public class ProducerStatistics {

	private String producer;
	private Integer interval;
	private Integer previousWin;
	private Integer followingWin;

	public ProducerStatistics(String producer, Integer interval, Integer previousWin, Integer followingWin) {
		super();
		this.producer = producer;
		this.interval = interval;
		this.previousWin = previousWin;
		this.followingWin = followingWin;
	}

	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public Integer getInterval() {
		return interval;
	}

	public void setInterval(Integer interval) {
		this.interval = interval;
	}

	public Integer getPreviousWin() {
		return previousWin;
	}

	public void setPreviousWin(Integer previousWin) {
		this.previousWin = previousWin;
	}

	public Integer getFollowingWin() {
		return followingWin;
	}

	public void setFollowingWin(Integer followingWin) {
		this.followingWin = followingWin;
	}
	
	@Override
	public String toString() {
		return "[producer=" + getProducer() + //
				" , interval=" + getInterval() + //
				" , previousWin=" + getPreviousWin() + //
				" , followingWin=" + getFollowingWin() +
				"]";
	}
}