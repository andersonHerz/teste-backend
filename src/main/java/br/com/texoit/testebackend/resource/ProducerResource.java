package br.com.texoit.testebackend.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.texoit.testebackend.repository.projection.ProducerStatisticsProjection;
import br.com.texoit.testebackend.service.ProducerService;

@RestController
@RequestMapping("/producer")
public class ProducerResource {

	@Autowired
	private ProducerService producerService;

	@GetMapping("/statistics")
	public ProducerStatisticsProjection listStatisticsWinners() {
		return producerService.listStatisticsWinners();
	}

}
