package br.com.texoit.testebackend.initialize;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;

public class ApplicationInitializerValidator {

	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationInitializerValidator.class);

	private static final int YEAR_INDEX = 0;
	private static final int TITLE_INDEX = 1;
	private static final int STUDIOS_INDEX = 2;
	private static final int PRODUCERS_INDEX = 3;
	private static final int WINNER_INDEX = 4;

	private static final String YEAR_HEADER = "year";
	private static final String TITLE_HEADER = "title";
	private static final String STUDIOS_HEADER = "studios";
	private static final String PRODUCERS_HEADER = "producers";
	private static final String WINNER_HEADER = "winner";

	private static final String FIELDS_REQUIRED = YEAR_HEADER + "; " + TITLE_HEADER + "; " + STUDIOS_HEADER + "; "
			+ PRODUCERS_HEADER;

	public static boolean validate(File file) throws IOException, CsvException {
		Reader reader = new BufferedReader(Files.newBufferedReader(file.toPath()));

		CSVReader csvReader = new CSVReaderBuilder(reader).build();
		List<String[]> readAll = csvReader.readAll();

		int lineNumber = 1;
		boolean isFirstLine = true;
		for (String[] lineData : readAll) {
			if (isFirstLine) {
				if (Boolean.FALSE.equals(validateHeaders(lineData))) {
					return false;
				}

				isFirstLine = false;
				lineNumber++;
				continue;
			}

			if (Boolean.FALSE.equals(validateData(lineData, lineNumber))) {
				return false;
			}

			lineNumber++;
		}

		return true;
	}

	private static boolean validateHeaders(String[] headers) {
		String header = "";
		for (String string : headers) {
			header = header.concat(string);
		}

		String[] splitHeaders = header.split(";");
		if (splitHeaders.length != 5) {
			LOGGER.error(
					"Verifique o número de campos do cabeçalho e também o separador. São esperados 5 campos e o separador \";\", foram informados {} campos.",
					splitHeaders.length);
			return false;
		}

		if (Boolean.FALSE.equals(splitHeaders[YEAR_INDEX].equalsIgnoreCase(YEAR_HEADER) && //
				splitHeaders[TITLE_INDEX].equalsIgnoreCase(TITLE_HEADER) && //
				splitHeaders[STUDIOS_INDEX].equalsIgnoreCase(STUDIOS_HEADER) && //
				splitHeaders[PRODUCERS_INDEX].equalsIgnoreCase(PRODUCERS_HEADER) && //
				splitHeaders[WINNER_INDEX].equalsIgnoreCase(WINNER_HEADER))) {
			LOGGER.error("São esperados os seguintes campos no cabeçalho e na seguinte ordem: {}; {}; {}; {}; {}.\n Campos informados: {}",
					YEAR_HEADER, TITLE_HEADER, STUDIOS_HEADER, PRODUCERS_HEADER, WINNER_HEADER, splitHeaders);
			return false;
		}

		return true;
	}

	private static boolean validateData(String[] data, int lineNumber) {
		String value = "";
		for (String string : data) {
			value = value.concat(string);
		}

		String[] splitValues = value.split(";");

		if (splitValues.length < 4) {
			LOGGER.error("Os campos {} são obrigatórios, verifique os dados informados.\n Linha: {} - Dados: {}",
					FIELDS_REQUIRED, lineNumber, data);
			return false;
		}

		return validateDataType(splitValues, YEAR_INDEX, lineNumber)
				&& validateDataType(splitValues, WINNER_INDEX, lineNumber);
	}

	private static boolean validateDataType(String[] splitValues, int index, int lineNumber) {
		switch (index) {
		case YEAR_INDEX:
			String year = splitValues[index];
			try {
				Integer.valueOf(year);
				return true;
			} catch (Exception e) {
				LOGGER.error(
						"Valor informado \"{}\" para o campo \"{}\" na linha {} é inválido. São esperados somente valores inteiros.",
						year, YEAR_HEADER, lineNumber);
				return false;
			}

		case WINNER_INDEX:
			try {
				String winner = splitValues[index];
				if (Boolean.FALSE.equals(winner.equalsIgnoreCase("yes") || //
						winner.equalsIgnoreCase("no") || //
						winner.equalsIgnoreCase("true") || //
						winner.equalsIgnoreCase("false"))) {

					LOGGER.error(
							"Valor informado \"{}\" para o campo \"{}\" na linha {} é inválido. São aceitos somente valores \"yes\", \"no\", \"true\" e \"false\".",
							winner, WINNER_HEADER, lineNumber);
					return false;
				}
			} catch (IndexOutOfBoundsException e) {
				// Do nothing: neste caso, como não foi informado valor para este campo será
				// lido com FALSE
			}
			return true;

		default:
			return true;
		}
	}

}
