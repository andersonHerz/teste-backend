package br.com.texoit.testebackend.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.texoit.testebackend.model.Producer;

public interface ProducerRepository extends JpaRepository<Producer, UUID> {

	public Optional<Producer> findByName(String name);

	public List<Producer> findByMoviesWinnerIsTrue();

}
