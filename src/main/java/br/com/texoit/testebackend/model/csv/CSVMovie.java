package br.com.texoit.testebackend.model.csv;

import com.opencsv.bean.CsvBindByName;


public class CSVMovie {

	@CsvBindByName(required = true)
	private Integer year;

	@CsvBindByName(required = true)
	private String title;

	@CsvBindByName(required = true)
	private String studios;

	@CsvBindByName(required = true)
	private String producers;

	@CsvBindByName
	private Boolean winner;

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStudios() {
		return studios;
	}

	public void setStudios(String studios) {
		this.studios = studios;
	}

	public String getProducers() {
		return producers;
	}

	public void setProducers(String producers) {
		this.producers = producers;
	}

	public Boolean isWinner() {
		return winner == null ? false : winner;
	}

	public void setWinner(Boolean winner) {
		this.winner = winner;
	}
	
	@Override
	public String toString() {
		return "[year= " + this.getYear() + //
				", title= " + this.getTitle() + //
				", studios= " + this.getStudios() + //
				", producers= " + this.getProducers() + //
				", winner= " + this.isWinner() + "]";
	}

}
