package br.com.texoit.testebackend.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.texoit.testebackend.model.Movie;
import br.com.texoit.testebackend.model.Producer;
import br.com.texoit.testebackend.repository.ProducerRepository;
import br.com.texoit.testebackend.repository.projection.ProducerStatistics;
import br.com.texoit.testebackend.repository.projection.ProducerStatisticsProjection;
import br.com.texoit.testebackend.service.ProducerService;

@Service
public class ProducerServiceImpl implements ProducerService {

	@Autowired
	private ProducerRepository repository;

	@Override
	@Transactional
	public Producer findByNameOrElseCreate(String name) {
		Optional<Producer> producer = repository.findByName(name);
		return producer.isPresent() ? producer.get() : repository.save(new Producer(name));
	}

	@Override
	@Transactional(readOnly = true)
	public List<Producer> findAllById(List<UUID> ids) {
		return repository.findAllById(ids);
	}

	@Override
	@Transactional(readOnly = true)
	public ProducerStatisticsProjection listStatisticsWinners() {
		List<Producer> producers = repository.findByMoviesWinnerIsTrue();

		Map<Producer, List<Movie>> mapProducerMoviesWinner = getMapProducerWinnerMovies(producers);
		List<ProducerStatistics> producerStatistics = getProducersStatistics(mapProducerMoviesWinner);

		List<ProducerStatistics> min = getMinInterval(producerStatistics);
		List<ProducerStatistics> max = getMaxInterval(producerStatistics);

		return new ProducerStatisticsProjection(min, max);
	}

	/**
	 * Retorna um mapa com os produtores que tenham mais de um filme vencedor e os
	 * respectivos filmes vencedores.
	 * 
	 * @param producers Produtores.
	 * @return Mapa de produtores e seus respectivos filmes vencedores.
	 */
	private Map<Producer, List<Movie>> getMapProducerWinnerMovies(List<Producer> producers) {
		Map<Producer, List<Movie>> map = new HashMap<>();

		for (Producer producer : producers) {
			if (!map.containsKey(producer)) {
				// Filtrar somente os filmes vencedores do produtor
				List<Movie> winnerMovies = producer.getMovies() //
						.stream() //
						.filter(Movie::isWinner) //
						.collect(Collectors.toList());

				// Adicionar no mapa somente produtores que tenham mais de um filme vencedor
				if (winnerMovies.size() > 1) {
					map.put(producer, winnerMovies);
				}
			}
		}

		return map;
	}

	/**
	 * Percorre o mapa, criando uma lista de estatísticas dos produtores com seus
	 * filmes vencedores.
	 * 
	 * @param mapProducerMoviesWinner Mapa de produtores com seus respectivos filmes
	 *                                vencedores.
	 * @return Lista de estatísticas dos produtores com filmes vencedores.
	 */
	private List<ProducerStatistics> getProducersStatistics(Map<Producer, List<Movie>> mapProducerMoviesWinner) {
		List<ProducerStatistics> producerStatistics = new ArrayList<>();

		for (Entry<Producer, List<Movie>> entry : mapProducerMoviesWinner.entrySet()) {
			List<Movie> movies = entry.getValue() //
					.stream() //
					.sorted((a, b) -> a.getYear().compareTo(b.getYear())) //
					.collect(Collectors.toList());

			int previousYear = 0;
			for (Movie movie : movies) {
				if (previousYear > 0) {
					int interval = movie.getYear() - previousYear;
					producerStatistics.add(
							new ProducerStatistics(entry.getKey().getName(), interval, previousYear, movie.getYear()));
				}

				previousYear = movie.getYear();
			}
		}

		return producerStatistics;
	}

	/**
	 * Retorna uma lista de estatísticas dos produtores que tenham o menor intervalo
	 * de tempo entre o ano de vencimento de seus filmes.
	 * 
	 * @param producerStatistics Lista de estatísticas dos produtores com filmes
	 *                           vencedores.
	 * @return Lista de estatísticas dos produtores com filmes vencedores.
	 */
	private List<ProducerStatistics> getMinInterval(List<ProducerStatistics> producerStatistics) {
		Optional<ProducerStatistics> minInterval = producerStatistics.stream() //
				.sorted((a, b) -> a.getInterval().compareTo(b.getInterval())) //
				.findFirst();

		return producerStatistics.stream() //
				.filter(ps -> ps.getInterval().equals(minInterval.get().getInterval())) //
				.collect(Collectors.toList());
	}

	/**
	 * Retorna uma lista de estatísticas dos produtores que tenham o maior intervalo
	 * de tempo entre o ano de vencimento de seus filmes.
	 * 
	 * @param producerStatistics Lista de estatísticas dos produtores com filmes
	 *                           vencedores.
	 * @return Lista de estatísticas dos produtores com filmes vencedores.
	 */
	private List<ProducerStatistics> getMaxInterval(List<ProducerStatistics> producerStatistics) {
		Optional<ProducerStatistics> maxInterval = producerStatistics.stream() //
				.sorted((a, b) -> b.getInterval().compareTo(a.getInterval())) //
				.findFirst();

		return producerStatistics.stream() //
				.filter(ps -> ps.getInterval().equals(maxInterval.get().getInterval())) //
				.collect(Collectors.toList());

	}

}
