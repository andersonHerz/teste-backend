package br.com.texoit.testebackend.service.impl;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.texoit.testebackend.model.Movie;
import br.com.texoit.testebackend.model.Producer;
import br.com.texoit.testebackend.repository.MovieRepository;
import br.com.texoit.testebackend.service.MovieService;
import br.com.texoit.testebackend.service.ProducerService;

@Service
public class MovieServiceImpl implements MovieService {

	@Autowired
	private MovieRepository repository;

	@Autowired
	private ProducerService producerService;

	@Override
	@Transactional
	public List<Movie> saveAll(List<Movie> movies) {
		for (Movie movie : movies) {
			List<UUID> ids = movie.getProducers() //
					.stream() //
					.map(Producer::getId) //
					.collect(Collectors.toList());

			List<Producer> producers = producerService.findAllById(ids);
			movie.setProducers(producers);
		}

		return repository.saveAll(movies);
	}

}
