# testebackend
Projeto desenvolvido para teste técnico em processo seletivo.

# Instruções para execução
### Projeto:
 - Informar no arquivo de propriedades do projeto(`application.properties`) na propriedade **api.location-file** a url do arquivo csv a ser carregado;
 - Executar via linha de comando(cmd) o comando `mvn install` na raiz do projeto;
 - Executar via linha de comando(cmd) o comando `mvn spring-boot:run` na raiz do projeto;
 - Por padrão será iniciado um servidor web na porta 8080, caso a mesma já esteja sendo utilizada é possível trocá-la informando no arquivo de propriedades(`application.properties`) a propriedade **server.port** e a porta desejada.

### Testes de integração
 - Para executar o teste de integração, basta executar via linha de comando(cmd) o comando `mvn test` na raiz do projeto;

# API's disponíveis
- GET - http://{url_servidor}:{porta_servidor}/producer/statistics : irá retornar o produtor com maior intervalo entre dois prêmios consecutivos, e o que obteve dois prêmios mais rápido.