package br.com.texoit.testebackend.service;

import java.util.List;

import br.com.texoit.testebackend.model.Movie;

public interface MovieService {

	public List<Movie> saveAll(List<Movie> movies);

}
