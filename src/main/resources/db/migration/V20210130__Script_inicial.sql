CREATE TABLE movie (
	id varchar(36) primary key,
	year int not null,
	title varchar(255) not null,
	studios varchar(255) not null,
	--producers varchar(255) not null,
	winner boolean not null
);

CREATE TABLE producer (
	id varchar(36) primary key,
	name varchar(255) not null
);

CREATE TABLE movie_producer (
	movie_id varchar(36) not null,
	producer_id varchar(36) not null,
    CONSTRAINT fk_movie FOREIGN KEY(movie_id) REFERENCES movie (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_producer FOREIGN KEY (producer_id) REFERENCES producer (id) ON DELETE CASCADE ON UPDATE CASCADE
);