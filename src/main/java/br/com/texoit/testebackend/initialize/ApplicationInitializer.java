package br.com.texoit.testebackend.initialize;

import java.io.BufferedReader;
import java.io.File;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

import br.com.texoit.testebackend.configuration.property.ApplicationApiProperty;
import br.com.texoit.testebackend.model.Movie;
import br.com.texoit.testebackend.model.Producer;
import br.com.texoit.testebackend.model.csv.CSVMovie;
import br.com.texoit.testebackend.service.MovieService;
import br.com.texoit.testebackend.service.ProducerService;

@Component
public class ApplicationInitializer {

	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationApiProperty.class);

	@Autowired
	private MovieService movieService;

	@Autowired
	private ProducerService producerService;

	public void initialize(String locationFile) {
		loadData(locationFile);
	}

	private void loadData(String locationFile) {
		try {
			File file = ResourceUtils.getFile(locationFile);

			if (ApplicationInitializerValidator.validate(file)) {
				Reader reader = new BufferedReader(Files.newBufferedReader(file.toPath()));

				CsvToBean<CSVMovie> csvToBean = new CsvToBeanBuilder<CSVMovie>(reader) //
						.withType(CSVMovie.class) //
						.withSeparator(';') //
						.build();

				List<CSVMovie> csvMovies = csvToBean.parse();
				LOGGER.info("Lista de filmes carregada com sucesso!!! \"{}\" filmes carregados.", csvMovies.size());

				writeData(csvMovies);
			} else {
				LOGGER.error("Falha ao validar o arquivo informado. Verifique os log da aplicação.");
			}
		} catch (NoSuchFileException e) {
			LOGGER.error("Arquivo CSV não encontrado, verifique o caminho informado: {}", locationFile);
		} catch (Exception e) {
			LOGGER.error("Ocorreu um erro inesperado ao carregar a lista de filmes. {}", e.getMessage());
		}
	}

	private void writeData(List<CSVMovie> csvMovies) {
		List<Movie> movies = converter(csvMovies);
		movieService.saveAll(movies);
	}

	private List<Movie> converter(List<CSVMovie> csvMovies) {
		List<Movie> movies = new ArrayList<Movie>();

		for (CSVMovie csvMovie : csvMovies) {
			List<String> listProducers = listProducers(csvMovie.getProducers());
			List<Producer> producers = new ArrayList<>();

			for (String strProducer : listProducers) {
				producers.add(producerService.findByNameOrElseCreate(strProducer));
			}

			movies.add(new Movie(csvMovie.getYear(), //
					csvMovie.getTitle(), //
					csvMovie.getStudios(), //
					producers, //
					csvMovie.isWinner()));
		}

		return movies;
	}

	public List<String> listProducers(String producers) {
		List<String> producerList = new ArrayList<>();

		if (producers.contains(",")) {
			String[] split = producers.split(",");
			int indexUltimo = 0;

			for (int i = 0; i < split.length - 1; i++) {
				if (StringUtils.isNotBlank(split[i])) {
					producerList.add(split[i].trim());
				}

				indexUltimo = i;
			}

			producerList.addAll(listProducers(split[indexUltimo + 1]));
		} else if (producers.contains(" and ")) {
			String[] split = producers.split(" and ");
			int indexUltimo = 0;

			for (int i = 0; i < split.length - 1; i++) {
				if (StringUtils.isNotBlank(split[i])) {
					producerList.add(split[i].trim());
				}

				indexUltimo = i;
			}

			producerList.addAll(listProducers(split[indexUltimo + 1]));
		} else {
			producerList.add(producers.trim());
		}

		return producerList;
	}

}
